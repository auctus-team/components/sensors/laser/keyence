 # Continous integration

[![pipeline status](https://gitlab.inria.fr/auctus/panda/keyence/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/panda/keyence/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=auctus:panda:keyence)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:keyence)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:keyence&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:keyence)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:keyence&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:keyence)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:keyence&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:keyence)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:keyence&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:keyence)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:keyence&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:keyence)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:keyence&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:keyence)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Akeyence
- Documentation : https://auctus.gitlabpages.inria.fr/panda/keyence/index.html


# Keyence

Package for reading the keyence SZV-32N laser scanner data and publish them as a ROS sensors_msgs/LaserScan Message

To test the code:
`roslaunch keyence keyence.launch`

Optional parameters :
- hostname : the laser IP adress
- port : the port on which to send/listen for the udp packet

The resulting laser message is published on the topic /scan 

